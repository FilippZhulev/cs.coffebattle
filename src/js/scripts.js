function full_screen() {
    var prfx = ["webkit", "moz", "ms", "o", ""]; function RunPrefixMethod(obj, method) { var p = 0, m, t; while (p < prfx.length && !obj[m]) { m = method; if (prfx[p] == "") { m = m.substr(0,1).toLowerCase() + m.substr(1); } m = prfx[p] + m; t = typeof obj[m]; if (t != "undefined") { prfx = [prfx[p]]; return (t == "function" ? obj[m]() : obj[m]); } p++; } }

    var btn = document.getElementById('startBtn');
    var elm = document.getElementById('full');

    btn.onclick = function() {
        if (RunPrefixMethod(document, "FullScreen") || RunPrefixMethod(document, "IsFullScreen")) {
             RunPrefixMethod(document, "CancelFullScreen");
        }
        else {
            RunPrefixMethod(elm, "RequestFullScreen");
        }
    }
}

$(window).ready(function() {

    var player_1_health = 100;
    var player_2_health = 100;


    var player_1 = $('._player-1');
    var player_2 = $('._player-2');

    var _event_1 = 0;
    var _event_3 = 0;
    var _event_4 = 0;


    full_screen();
    $('.loading_screen h1').addClass('ready');

    setTimeout(function(){
        $('.loading_screen h1 strong').addClass('ready');
    }, 3500);
    setTimeout(function(){
        $('.start-btn').addClass('ready');
    }, 6000);
    var audio = $(".load-sound")[0];
    audio.play();

    $('.start-btn').click(function(){
        setTimeout(function(){
            $('.loading_screen').addClass('start');
            go_game ()
        }, 1000)
    });

    function go_game () {

        var i = 0;

        var main = $('main');

        var player_1_point = 0;
        var player_2_point = 0;

        var event = 0;

        function set_point(player, point) {
            if(player_1_health > 0 && player_2_health > 0){
                if(player == 1) {
                    player_1_point += point;
                    $('.player_1_point').val(player_1_point);
                    $('.scope_player-1 > i').html(player_1_point);
                }

                if(player == 2) {
                    player_2_point += point;
                    $('.player_2_point').val(player_2_point);
                    $('.scope_player-2 > i').html(player_2_point)
                }
            }
        }

        function re_health(a,b) {
            if(a == '.player-h_1') {

                player_1_health = player_1_health - b;
                $(a).addClass('active').find('.health-line_inner').css({width: player_1_health + '%'});
                setTimeout(function() {
                    $(a).removeClass('active').find('.health-line_inner-2').css({width: player_1_health + '%'});
                },500);

                player_1.addClass('dead');
                setTimeout(function(){
                    player_1.removeClass('dead');
                },100);
            }
            if(a == '.player-h_2') {

                player_2_health = player_2_health - b;
                $(a).addClass('active').find('.health-line_inner').css({width: player_2_health + '%'});
                player_2.addClass('dead');
                setTimeout(function() {
                    $(a).removeClass('active').find('.health-line_inner-2').css({width: player_2_health + '%'});
                },500);

                setTimeout(function(){
                    player_2.removeClass('dead');
                },100);
            }
        }
        function dead() {
            if(player_1_health <= 0) {
                player_1.addClass('player_dead');
            }
            if(player_2_health <= 0) {
                player_2.addClass('player_dead');
            }
        }

        var events = {};


        document.addEventListener('keydown', function (e) {
          events[e.keyCode] = true;
          action();
        });

        document.addEventListener('keyup', function (e) {
          events[e.keyCode] = false;
        });

        function get_shot(a) {
            if(a > 0) {
                var player_1_pos = player_1.offset();
                var player_2_pos = player_2.offset();

                var shot_pos = 0;

                _event_3 = 1;

                i++;

                var shot_pos_left = 0;

                var shot_first_pos = player_1_pos.top + 50;
                $('main').append('<div class="shot sh_' + i + '" style="left: ' + player_1_pos.left + 'px;top:' + shot_first_pos + 'px"></div>');
                setTimeout(function () {
                    var shot = $('.shot');
                    shot_pos = shot.offset();
                    if (player_1_pos.left <= player_2_pos.left) {
                        shot_pos_left = shot_pos.left + player_2_pos.left + 1000;
                        shot.animate({left: shot_pos_left}, {
                            duration: 2000,
                            step: function () {
                                $('html').on('on.bam', function () {
                                    if (shot.offset().left > player_2.offset().left - 203 && shot.offset().left > 1) {
                                        shot.remove();
                                        set_point(2, 125);
                                        set_point(1, 25);
                                    }
                                });
                                if (shot.offset().left > player_2.offset().left && shot.offset().left < player_2.offset().left + 350 && player_2.offset().top > $('main').height() - 250 && shot.offset().left > 1) {
                                    $(".shot.sh_" + i).remove();
                                    re_health('.player-h_2', 25);
                                    set_point(1, 125);
                                    dead();
                                }
                            }
                        });
                    }
                    else {
                        shot_pos_left = shot_pos.left - player_2_pos.left - 1000;
                        shot.animate({left: shot_pos_left}, {
                            duration: 2000,
                            step: function () {
                                $('html').on('on.bam', function () {
                                    if (shot.offset().left < player_2.offset().left + 203 && shot.offset().left > 1) {
                                        shot.remove();
                                        set_point(2, 125);
                                        set_point(1, 25);
                                    }
                                });
                                if (shot.offset().left < player_2.offset().left + 100 && shot.offset().left > player_2.offset().left - 50 && player_2.offset().top > $('main').height() - 250 && shot.offset().left > 1) {
                                    $(".shot.sh_" + i).remove();
                                    re_health('.player-h_2', 25);
                                    set_point(1, 125);
                                    dead();
                                }
                            }
                        });
                    }
                }, 10);
                setTimeout(function () {
                    _event_3 = 0;
                    $(".shot.sh_" + i).remove();
                }, 2010);
            }
        }

        function get_jump(a,b) {
            if(event == 0 && b > 0) {
                event = 1;
                a.css({bottom: '150px'});
                setTimeout(function(){
                    a.css({bottom: '0px'});
                },300);
                setTimeout(function(){
                    event = 0;
                },600);
            }
        }

        function get_right(a,b) {
            if(b > 0 && a.offset().left < main.width() - 103) {
                var player_pos = a.offset();
                _event_1 = 1;
                player_pos.left = player_pos.left + 60;
                a.css({left: player_pos.left});
                setTimeout(function(){
                    _event_1 = 0;
                },300);
            }
        }

        function get_left(a,b) {
            if(b > 0 && a.offset().left > 103) {
                var player_pos = a.offset();

                _event_1 = 1;
                player_pos.left = player_pos.left - 60;
                a.css({left: player_pos.left});
                setTimeout(function () {
                    _event_1 = 0;
                }, 300);
            }
        }

        function get_bam(a) {
            if(a > 0) {
                var player_1_pos = player_1.offset();
                var player_2_pos = player_2.offset();


                _event_4 = 1;
                if (player_1_pos.left < player_2_pos.left) {
                    $('html').trigger('on.bam');
                    if (player_1.offset().left + 200 > player_2.offset().left && player_1.offset().left < player_2.offset().left + 100 && player_1.offset().top >= main.height() - 200) {
                        re_health('.player-h_1', 50);
                        set_point(2, 200);
                        dead();
                    }
                    if (player_1.offset().left + 200 > player_2.offset().left && player_1.offset().left < player_2.offset().left + 100 && player_1.offset().top <= main.height() - 200) {
                        set_point(1, 50);
                    }
                    setTimeout(function () {
                        _event_4 = 0;
                    }, 200);
                } else {
                    $('html').trigger('on.bam');
                    if (player_1.offset().left - 306 < player_2.offset().left && player_1.offset().left < player_2.offset().left + 100 && player_1.offset().top >= main.height() - 200) {
                        re_health('.player-h_1', 50);
                        set_point(2, 200);
                        dead();
                    }
                    if (player_1.offset().left - 306 < player_2.offset().left && player_1.offset().left < player_2.offset().left + 100 && player_1.offset().top <= main.height() - 200) {
                        set_point(1, 50);
                    }
                    setTimeout(function () {
                        _event_4 = 0;
                    }, 200);
                }
            }
        }

        function action () {

          if (events[38]) {
              get_jump(player_1, player_1_health);
          }
          if (events[87]) {
              get_jump(player_2, player_2_health);
          }
          if (events[39]) {
              get_right(player_1, player_1_health);
          }
          if (events[68]) {
              get_right(player_2, player_2_health);
          }
          if (events[37]) {
              get_left(player_1, player_1_health);
          }
          if (events[65]) {
              get_left(player_2, player_2_health);
          }
          if(events[32] && _event_4 == 0) {
              get_bam(player_2_health)
          }
          if (events[45] && _event_3 == 0) {
              get_shot(player_1_health);
          }
        }

        function get_bot() {

            function set_random(min, max) {
                return Math.floor(Math.random() * (max - min)) + min;
            }
            function bot_player_1() {
                setInterval(function(){
                    if(set_random(1,1) == 1) {
                        get_shot(player_1_health);
                    }
                }, 2020);

                setInterval(function(){
                    if(player_2.offset().left - 200 < player_1.offset().left + 400) {
                        if(set_random(1,3) == 2) {
                            get_jump(player_1, player_1_health);
                        }
                    }else {
                        if(set_random(1,8) == 4) {
                            get_jump(player_1, player_1_health);
                        }
                    }
                }, 500);
                setInterval(function(){
                    if(set_random(1,20) == 7) {
                        get_right(player_1, player_1_health);
                    }
                }, 100);
                setInterval(function(){
                    if(set_random(1,25) == 7) {
                        get_left(player_1, player_1_health);
                    }
                }, 100);
            }


            function bot_player_2() {
                setInterval(function(){
                    if(set_random(1,4) != 2){
                        get_bam(player_2_health);
                    }
                }, 100);

                setInterval(function(){
                    if(player_2.offset().left - 200 < player_1.offset().left) {
                        get_bam(player_2_health);
                    }
                }, 500);

                setInterval(function(){
                    if(set_random(1,8) == 4) {
                        get_jump(player_2, player_2_health);
                    }
                }, 500);

                setInterval(function(){
                    if(player_2.offset().left >= 100) {
                        if(player_2.offset().left > player_1.offset().left) {
                            if(set_random(1,55) == 10) {
                                get_right(player_2, player_2_health);
                            }
                        }else {
                            if(set_random(1,15) == 7) {
                                get_right(player_2, player_2_health);
                            }
                        }
                    }else {
                        if(set_random(1,15) == 10) {
                            get_right(player_2, player_2_health);
                        }
                    }
                }, 100);

                setInterval(function(){
                    if(player_2.offset().left >= 100) {
                        if(player_2.offset().left > player_1.offset().left) {
                            if(set_random(1,8) == 7) {
                                get_left(player_2, player_2_health);
                            }
                        }else {
                            if(set_random(1,45) == 7) {
                                get_left(player_2, player_2_health);
                            }
                        }
                    }
                }, 100);
            }

            var $bot = $('.this_bot');

            if($bot.val() == 1) {
                bot_player_1();
            }
            bot_player_2();
        }

        get_bot();

    }
});
